from django.urls import path, include

from . import views as landing_views

urlpatterns = [
    path('', landing_views.home_view, name='landing-home'),
    path('login/', landing_views.MyLoginView.as_view(), name='landing-login'),
    path('logout/', landing_views.MyLogoutView.as_view(), name='landing-logout'),
]