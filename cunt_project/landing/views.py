from django.shortcuts import render
from django.contrib.auth import views as auth_views
from django.contrib.admin.views.decorators import staff_member_required

# Create your views here.

class MyLoginView(auth_views.LoginView):
    template_name = 'landing/login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'placeholder':'placeholder'
        })

        context['form']['username'].label = 'Usuário'
        context['form']['password'].label = 'Senha'
        
        return context

class MyLogoutView(auth_views.LogoutView):
    template_name = 'landing/logout.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'placeholder':'placeholder'
        })
        return context

def home_view(request, *args, **kwargs):
    template_name = 'landing/home.html'

    perm = None

    my_context = {
        'projeto': 'landing page',
        'permissions': perm
    }
    return render(request, template_name, my_context)