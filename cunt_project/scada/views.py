from django.shortcuts import render

import paho.mqtt.client as mqtt

# Create your views here.

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))



def dashboard_view(request, *args, **kwargs):
    template_name = 'scada/dashboard.html'

    client = mqtt.Client()
    client.on_message = on_message

    my_context = {}
    return render(request, template_name, my_context)