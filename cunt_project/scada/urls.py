from django.urls import path, include

from . import views as scada_views

urlpatterns = [
    path('', scada_views.dashboard_view, name='scada-dashboard'),
]